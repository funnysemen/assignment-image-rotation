#include "files_io.h"
#include "image.h"
#include "input_formats.h"
#include "output_formats.h"
#include "rotation.h"
#include <stdio.h>


int main( int argc, char** argv ) {
    if(argc < 2) {
        printf("Invalid number of arguments/n");
        return 1;
    }
    FILE *source = fopen(argv[1], "r+");
    struct image img = {0};
    enum read_status s = from_bmp(source, &img);
    read_echo(s);
    close_echo(close(source));
    if(s == READ_OK) {
        struct image rotated_img = rotate_270(img);
        free(img.data);
        FILE *out = fopen(argv[2], "w+");
        write_echo(to_bmp(out, &rotated_img));
        close_echo(close(out));
        free(rotated_img.data);
        rotated_img.data = NULL;
    }
    img.data = NULL;
    return 0;
}
