//
// Created by szymanski on 21.12.2021.
//

#include "output_formats.h"
#include <stdint.h>

enum write_status to_bmp( FILE* out, struct image const* img ){
    struct bmp_header header = image_header(img);
    if(fwrite(&header, sizeof(struct bmp_header), 1, out) != 1) return HEADER_WRITE_ERROR;
    uint8_t padding_byte = 0;
    uint32_t padding = get_padding(header.biWidth);
    uint64_t k = 0;



    for(uint64_t i = 0; i < img->height; i++){
        for(uint64_t j = 0; j < img->width; j++){
            if(fwrite(img->data + k, sizeof(struct pixel), 1, out) != 1) return PADDING_WRITE_ERROR;
            k++;
        }
        for (int i = 0; i < padding; i++) {
            if (fwrite(&padding_byte, sizeof(uint8_t), 1, out) != 1) return DATA_WRITE_ERROR;
        }
    }
    return WRITE_OK;
}

void write_echo(enum write_status s){
    fprintf(stderr,"%s", write_status_array[s]);
}
