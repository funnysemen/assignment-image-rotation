//
// Created by szymanski on 22.12.2021.
//

#include "files_io.h"

enum close_status close( FILE* out){
    if(fclose(out)) return CLOSE_ERROR;
    return CLOSE_OK;
}

void close_echo(enum close_status s){
    fprintf(stderr,"%s", close_status_array[s]);
}
