//
// Created by szymanski on 21.12.2021.
//
#include "input_formats.h"


struct bmp_header image_header(const struct image* img){
    uint32_t width = (uint32_t) img->width;
    uint32_t height = (uint32_t) img->height;
    struct bmp_header header = {
            .bfType = val_bfType,
            .bfileSize = (width * sizeof(struct pixel) + get_padding(width)) * height + sizeof(struct bmp_header),
            .bfReserved = val_bfReserved,
            .bOffBits = sizeof(struct bmp_header),
            .biSize = val_biSize,
            .biWidth = width,
            .biHeight = height,
            .biPlanes = val_biPlanes,
            .biBitCount = val_biBitCount,
            .biCompression = val_biCompression,
            .biSizeImage = (width * sizeof(struct pixel) + get_padding(width)) * height,
            .biXPelsPerMeter = val_biXPelsPerMeter,
            .biYPelsPerMeter = val_biYPelsPerMeter,
            .biClrUsed = val_biClrUsed,
            .biClrImportant = val_biClrImportant
    };
    return header;
}

uint32_t get_padding(uint32_t width){
    return (4 - sizeof(struct pixel) * width % 4) % 4;
}

enum read_status check_header(struct bmp_header * header){
    if(header->biBitCount != val_biBitCount) return READ_INVALID_BITS;
    if(header->bfType != val_bfType) return READ_INVALID_SIGNATURE;
    return READ_OK;
}

enum read_status from_bmp( FILE* in, struct image* img ) {
    struct bmp_header header;
    if(fread(&header, sizeof(struct bmp_header), 1, in) != 1) return READ_INVALID_HEADER;
    if(check_header(&header)) return check_header(&header);
    *img = init_image((uint64_t) header.biWidth, (uint64_t) header.biHeight);
    uint32_t padding = get_padding(header.biWidth);

    uint64_t k = 0;
        for (uint64_t i = 0; i < img->height; i++) {
            for (uint64_t j = 0; j < img->width; j++) {
                if(fread(img->data + k, sizeof(struct pixel), 1, in) != 1) return READ_INVALID_BITS;
                k++;
            }
            fseek(in, padding, SEEK_CUR);
        }

    return READ_OK;
}

void read_echo(enum read_status s){
    fprintf(stderr,"%s", read_status_array[s]);
}
