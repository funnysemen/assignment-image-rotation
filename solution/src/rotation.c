//
// Created by szymanski on 21.12.2021.
//

#include "image.h"
#include <stdio.h>

struct image rotate_90( struct image const img ){
    struct image rotated_img;
    rotated_img = init_image(img.height, img.width);
    if (rotated_img.data) {
        for (int i = 0; i < img.height; i++) {
            for (int j = 0; j < img.width; j++) {
                rotated_img.data[(img.width - j - 1) * img.height + i] = img.data[i * img.width + j];
            }
        }
    } else printf("Allocation ERROR/n");
    return rotated_img;
}

struct image rotate_270( struct image const img ){
    struct image rot_90 = rotate_90(img);
    struct image rot_180 = rotate_90(rot_90);
    free(rot_90.data);
    rot_90.data = NULL;
    struct image rotated_img = rotate_90(rot_180);
    free(rot_180.data);
    rot_180.data = NULL;
    return rotated_img;
}
