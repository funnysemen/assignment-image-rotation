//
// Created by szymanski on 21.12.2021.
//
#include "image.h"

struct image init_image(uint64_t w, uint64_t h){
    struct image img =  {
            .width = w,
            .height = h,
            .data = malloc(w * h * sizeof(struct pixel))
    };
    return img;
}
