//
// Created by szymanski on 22.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_FILES_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_FILES_IO_H
#include <stdio.h>

enum close_status  {
    CLOSE_OK = 0,
    CLOSE_ERROR,
};

static const char* close_status_array[] = {
        [CLOSE_OK] = "SUCCESS: closing file\n",
        [CLOSE_ERROR] = "ERROR: unable to close the file\n"
};

enum close_status close( FILE* out);

void close_echo(enum close_status s);

#endif //ASSIGNMENT_IMAGE_ROTATION_FILES_IO_H
