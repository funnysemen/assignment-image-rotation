//
// Created by szymanski on 21.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_INNER_FORMATS_H
#define ASSIGNMENT_IMAGE_ROTATION_INNER_FORMATS_H
#include <stdint.h>
#include <stdlib.h>


struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

struct image init_image(uint64_t w, uint64_t h);


#endif //ASSIGNMENT_IMAGE_ROTATION_INNER_FORMATS_H
