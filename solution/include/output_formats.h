//
// Created by szymanski on 21.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_OUTPUT_FORMATS_H
#define ASSIGNMENT_IMAGE_ROTATION_OUTPUT_FORMATS_H
#include "image.h"
#include "input_formats.h"
#include <stdio.h>


enum  write_status  {
    WRITE_OK = 0,
    HEADER_WRITE_ERROR,
    PADDING_WRITE_ERROR,
    DATA_WRITE_ERROR
};

static const char* write_status_array[] = {
        [WRITE_OK] = "SUCCESS: writing\n",
        [HEADER_WRITE_ERROR] = "ERROR: unable to write the header\n",
        [PADDING_WRITE_ERROR] = "ERROR: unable to write the padding\n",
        [DATA_WRITE_ERROR] = "ERROR: unable to write the data\n",
};

enum write_status to_bmp( FILE* out, struct image const* img );

void write_echo(enum write_status s);

#endif //ASSIGNMENT_IMAGE_ROTATION_OUTPUT_FORMATS_H
