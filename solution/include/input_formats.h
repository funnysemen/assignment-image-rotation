//
// Created by szymanski on 21.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_INPUT_FORMATS_H
#define ASSIGNMENT_IMAGE_ROTATION_INPUT_FORMATS_H
#include "image.h"
#include <stdio.h>
#include <stdint.h>

static const uint16_t val_bfType = 0x4D42;
static const uint32_t val_bfReserved = 0;
static const uint32_t val_biSize = 40;
static const uint16_t val_biPlanes = 1;
static const uint16_t val_biBitCount = 24;
static const uint32_t val_biCompression = 0;
static const uint32_t val_biXPelsPerMeter = 0;
static const uint32_t val_biYPelsPerMeter = 0;
static const uint32_t val_biClrUsed = 0;
static const uint32_t val_biClrImportant = 0;


struct __attribute__((packed)) bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};

struct bmp_header image_header(const struct image* img);

uint32_t get_padding(uint32_t width);

enum read_status  {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER
};

static const char* read_status_array[] = {
        [READ_OK] = "SUCCESS: reading\n",
        [READ_INVALID_SIGNATURE] = "ERROR: invalid signature during reading\n",
        [READ_INVALID_BITS] = "ERROR: incorrect number of bits per pixel\n",
        [READ_INVALID_HEADER] = "ERROR: incorrect BMP-file header\n",
};

enum read_status check_header(struct bmp_header* header);

enum read_status from_bmp(FILE* in, struct image* img);

void read_echo(enum read_status s);

#endif //ASSIGNMENT_IMAGE_ROTATION_INPUT_FORMATS_H
