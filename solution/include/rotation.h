//
// Created by szymanski on 21.12.2021.
//
#ifndef ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
#define ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
#include "image.h"
#include <stdio.h>

struct image rotate_270( struct image const img );

#endif //ASSIGNMENT_IMAGE_ROTATION_TRANSFORMATIONS_H
